package com.datahole.suffixarray.Main;

import java.util.Map;

import org.apache.log4j.Logger;

import com.datahole.suffixarray.SuffixArray;
import com.datahole.suffixarray.SuffixArrayByDC3;
import com.datahole.suffixarray.model.InitTuple;
import com.datahole.suffixarray.model.ResultTuple;
import com.datahole.suffixarray.model.Suffix;
import com.datahole.suffixarray.util.StringUtil;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class Main {

	private static Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args) {

		try {
			StringBuffer text = new StringBuffer("bananabanana");
			Multimap<String, Float> multiMap = ArrayListMultimap.create();
			Statistic ta = new Statistic();
			SuffixArray sa = new SuffixArrayByDC3();
			Suffix suffix = sa.solve(text);
			suffix.calcHeightArray(text, suffix);
			ta.calcWordCount(text.toString(), suffix, false);
			Map<String, ResultTuple> r_map = ta.getMap();
			System.out.println(r_map);
			ta.calcWordCount(text.reverse().toString(), suffix, true);
			Map<String, ResultTuple> l_map = ta.getMap();
			System.out.println(l_map);
			Map<String, InitTuple> imap = ta.intersectionMap(r_map, l_map); // 求交集
			Map<String, Integer> umap = ta.unionMap(r_map, l_map); // 求并集

			ta.calcSMI(multiMap, imap, umap);
			ta.calcAV(multiMap, imap);
			System.out.println(multiMap);

		} catch (Exception e) {
			logger.error(StringUtil.getTrace(e));
		}
	}

}
