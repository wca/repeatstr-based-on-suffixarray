package com.datahole.suffixarray;

import com.datahole.suffixarray.model.Suffix;

/**
 * 后缀数组求解类
 * @author hlyue
 *
 */
public interface SuffixArray {

	public Suffix solve(StringBuffer text);
}
