package com.datahole.suffixarray.model;

import java.util.*;


/**
 * 求交集之后的备选词value值
 * @author hlyue
 *
 */
public class InitTuple {

	public int frequncy ;
	public List<Integer> rav_list;
	public List<Integer> lav_list;
	
	public InitTuple(int frequncy,List<Integer> rav_list,List<Integer> lav_list){
		this.frequncy = frequncy;
		this.rav_list = rav_list;
		this.lav_list = lav_list;
	}
	
	@Override
	public String toString(){
		return " "+frequncy+":"+rav_list.toString()+":"+lav_list.toString();
		
	}
}
