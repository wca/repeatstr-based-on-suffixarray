package com.datahole.suffixarray.model;

import java.util.List;

/**
 * 重复串发现算法过程使用的数据结构
 * @author hlyue
 *
 */
public class ResultTuple {

	public int id;
	public int frequncy;
	public List<Integer> av;

	public ResultTuple(int id, int fre, List<Integer> av) {
		this.id = id;
		this.frequncy = fre;
		this.av = av;
	}
	
	public void reset(int id, int fre, List<Integer> av){
		this.id = id;
		this.frequncy = fre;
		this.av = av;
	}
	
	@Override
	public String toString(){
		return this.frequncy+":"+av.toString();
		
	}
}
